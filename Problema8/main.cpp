#include <iostream>

using namespace std;
//Programa que recibe una cadena de caracteres y separa los números del resto de caracteres
int main()
{string cadena_original;char numeros[10]={'0','1','2','3','4','5','6','7','8','9'};char numeros_cadena[1000]={};char caracteres_cadena[1000]={};
    char caracter;int contador=0,contador2=0,caracter_ascii;
    cout << "Ingrese la cadena de caracteres que quiera agregar: " << endl;
    cin >> cadena_original ;

    for(int i=0;i<(cadena_original.length());i++){//ciclo que vuelve los caracteres de la cadena a ascii para poder saber si son numeros o letras y los guarda en una variable
         caracter=cadena_original[i];
         caracter_ascii=int(caracter);

             if (caracter_ascii>=48  && caracter_ascii<=57 ){//condicional que se hace si la cadena es un numero y guarda los numeros en un arreglo
             numeros_cadena[contador]+=cadena_original[i];
             contador++;

             }
    }

   for(int i=0;i<(cadena_original.length());i++){//ciclo que hace lo mismo que arriba, pero esta vez con los caracteres no numericos
        caracter=cadena_original[i];
        caracter_ascii=int(caracter);

            if (caracter_ascii>57  || caracter_ascii<48 ){
            caracteres_cadena[contador2]+=cadena_original[i];
            contador2++;

            }
   }

cout<<"la parte numerica de la cadena es: "<<numeros_cadena<<" y la otra parte de los caracteres no numericos son: "<<caracteres_cadena<<endl;

    return 0;
}
